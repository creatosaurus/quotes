import React, { useContext } from 'react'
import '../ReusableComponentsCss/RightSaved.css'
import AppContext from '../../store/DataProvider'

const RightSaved = () => {

    const context = useContext(AppContext)

    return (
        <div className="right-saved-filter">
            <span className="head">Filter Category</span>
            <select onChange={(e)=> context.changeFilter(e.target.value)}>
                <option value="all">All</option>
                {
                    context.groupNames.map(data => {
                        return <option key={data} value={data}>{data}</option>
                    })
                }
            </select>
        </div>
    )
}

export default RightSaved
