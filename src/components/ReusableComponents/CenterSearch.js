import React, { useEffect, useContext, useState } from 'react'
import '../ReusableComponentsCss/CenterSearch.css'
import Search from '../../assets/Search.svg'
import AppContext from '../../store/DataProvider'
import axios from 'axios'
import decodeToken from "jwt-decode";
import Masonry from "react-responsive-masonry"
import constant from '../../constant'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Cross from '../../assets/Cross.svg'
import SavePopup from './SavePopup'

const CenterSearch = () => {

    const context = useContext(AppContext)

    const [selectId, setselectId] = useState(null)
    const [selectedCategory, setselectedCategory] = useState(null)
    const [loading, setloading] = useState(false)

    useEffect(() => {
        context.getSearchQuotes() // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const coptText = (text, author) => {
        let copyText = document.getElementById("copy");
        copyText.value = text + "  -" + author
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Quote copied", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const saveQuoteInGroup = async (name, quote, author) => {
        try {
            setselectedCategory(name)
            setloading(true)

            const decoded = decodeToken(localStorage.getItem("token"))
            const organizationId = localStorage.getItem('organizationId')

            const res = await axios.post(`${constant.url}save`, {
                organizationId: organizationId,
                userId: decoded.id,
                userName: decoded.userName,
                groupName: name,
                quote: quote,
                author: author,
            })

            setloading(false)
            setselectId(null)
            setselectedCategory(null)
            toast(`Quote saved to ${name} successfully`, {
                autoClose: 1000,
                hideProgressBar: false,
            });
            context.updateSavedQuote(res.data.data)
        } catch (error) {
            setloading(false)
            toast.error(`${error.response.data.error} in ${name} category`, {
                autoClose: 2000,
                hideProgressBar: false,
            });
        }
    }

    const scrollStarted = (e) => {
        if (e.target.scrollTop + e.target.clientHeight >= e.target.scrollHeight - 50) {
            if (context.quotesLoading || context.quotesFinished) return
            context.getSearchQuotesNext()
        }
    }


    const selectIdChange = () => {
        setselectId(null)
        setselectedCategory(null)
    }


    const Loading = () => {
        return <div className='scroll-container-new' style={{ display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)', gap: 10 }}>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div className='loading-card' style={{ width: '100%' }} key={data +  "centersearch"}>
                        <div className='skeleton' style={{ width: "100%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                    </div>
                })
            }
        </div>
    }

    return (
        <React.Fragment>
            <div className="search-container-center">
                <input type="text" value="" id="copy" onChange={() => console.log()} />
                <div className="input-container">
                    <form onSubmit={(e) => {
                        e.preventDefault()
                        context.getSearchQuotes()
                    }}>
                        <input type="text"
                            value={context.quoteQuery}
                            onChange={(e) => context.changeQuoteQuery(e.target.value)}
                            placeholder="Search topics and categories" />
                    </form>
                    <img className='search' src={Search} alt="" />
                    {
                        context.quoteQuery.trim() !== "" ?
                            <img
                                onClick={() => {
                                    context.changeQuoteQuery("")
                                    context.getSearchQuotes("")
                                }}
                                className='cross'
                                src={Cross} alt="" /> :
                            null
                    }
                </div>
  
                <div className='scroll-container' onScroll={scrollStarted}>
                    <Masonry columnsCount={3} gutter="20px">
                        {
                            context.quotes.map((data, index) => {
                                return <div className="card" key={data._id + index}>
                                    <p>{data.Queote}</p>
                                    <div className="author-container">
                                        <span>- {data.Author}</span>
                                        <div className="functions">
                                            <svg onClick={() => {
                                                if (!context.canEdit) return toast(`You have only view permission`, {
                                                    autoClose: 2000,
                                                    hideProgressBar: false,
                                                });
                                                setselectId(data._id)
                                            }} style={{ cursor: 'pointer', height: 20, width: 20 }} className='saved' xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill='none'>
                                                <path d="M22 8.86222C22 10.4087 21.4062 11.8941 20.3458 12.9929C17.9049 15.523 15.5374 18.1613 13.0053 20.5997C12.4249 21.1505 11.5042 21.1304 10.9488 20.5547L3.65376 12.9929C1.44875 10.7072 1.44875 7.01723 3.65376 4.73157C5.88044 2.42345 9.50794 2.42345 11.7346 4.73157L11.9998 5.00642L12.2648 4.73173C13.3324 3.6245 14.7864 3 16.3053 3C17.8242 3 19.2781 3.62444 20.3458 4.73157C21.4063 5.83045 22 7.31577 22 8.86222Z" stroke="#000" strokeLinejoin="round" />
                                            </svg>

                                            <svg onClick={() => coptText(data.Queote, data.Author)} style={{ cursor: 'pointer' }} width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16.1667 16.6667H8C7.72386 16.6667 7.5 16.4428 7.5 16.1667V8C7.5 7.72386 7.72386 7.5 8 7.5H16.1667C16.4428 7.5 16.6667 7.72386 16.6667 8V16.1667C16.6667 16.4428 16.4428 16.6667 16.1667 16.6667Z" stroke="black" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                                                <path d="M12.4997 7.50065V3.83398C12.4997 3.55784 12.2758 3.33398 11.9997 3.33398H3.83301C3.55687 3.33398 3.33301 3.55784 3.33301 3.83398V12.0006C3.33301 12.2768 3.55687 12.5007 3.83301 12.5007H7.49967" stroke="black" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                    </div>

                                    <div style={{ position: 'relative' }}>
                                        <SavePopup
                                            loading={loading}
                                            selectId={selectId}
                                            selectedCategory={selectedCategory}
                                            saveQuoteInGroup={saveQuoteInGroup}
                                            selectIdChange={selectIdChange}
                                            data={data} />
                                    </div>
                                </div>
                            })
                        }
                    </Masonry>
                    {
                        context.quotesLoading === true ? <Loading /> :
                            context.quotes.length === 0 ? <div>No search results were found. Please check your keyword or spelling and try again.</div> :
                                null
                    }
                </div>
            </div>
            <ToastContainer />
        </React.Fragment>
    )
}

export default CenterSearch