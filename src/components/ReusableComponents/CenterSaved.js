import axios from 'axios'
import React, { useContext, useState, useEffect } from 'react'
import AppContext from '../../store/DataProvider'
import '../ReusableComponentsCss/CenterSaved.css'
import constant from '../../constant';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SavePopup from './SavePopup';
import decodeToken from "jwt-decode";

const CenterSaved = () => {

   const context = useContext(AppContext)
   const [deletedId, setdeletedId] = useState(null)
   const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
   const [selectedId, setselectedId] = useState(null)
   const [loading, setloading] = useState(false)
   const [selectedCategory, setselectedCategory] = useState(null)

   useEffect(() => {
      context.getSavedQuotes() // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [])

   const copyText = (text, author) => {
      let copyText = document.getElementById("copySaved");
      copyText.value = text + "  -" + author
      copyText.select();
      copyText.setSelectionRange(0, 99999);
      document.execCommand("copy");
      toast("Quote copied", {
         autoClose: 1000,
         hideProgressBar: false,
      });
   }

   const deleteSavedQuote = async (id) => {
      try {
         if (!context.canEdit) return toast(`You have only view permission`, {
            autoClose: 2000,
            hideProgressBar: false,
         });

         const organizationId = localStorage.getItem('organizationId')
         setdeletedId(id)
         await axios.delete(`${constant.url}save/remove/${id}/${organizationId}`)

         toast("Quote deleted successfully", {
            autoClose: 1000,
            hideProgressBar: false,
         });
         setdeletedId(null)
         context.removeSavedQuote(id)
      } catch (error) {
         setdeletedId(null)
      }
   }

   const getFormatedDate = (date) => {
      date = new Date(date)
      return date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear()
   }

   const selectQuote = (data) => {
      if (!context.canEdit) return toast(`You have only view permission`, {
         autoClose: 2000,
         hideProgressBar: false,
      });
      setselectedId(data._id)
   }

   const saveQuoteInGroup = async (name, quote, author) => {
      try {
         setselectedCategory(name)
         setloading(true)

         const decoded = decodeToken(localStorage.getItem("token"))
         const organizationId = localStorage.getItem('organizationId')

         const res = await axios.post(`${constant.url}save`, {
            organizationId: organizationId,
            userId: decoded.id,
            userName: decoded.userName,
            groupName: name,
            quote: quote,
            author: author,
         })

         setloading(false)
         setselectedId(null)
         setselectedCategory(null)
         toast(`Quote saved to ${name} successfully`, {
            autoClose: 1000,
            hideProgressBar: false,
         });
         context.updateSavedQuote(res.data.data)
      } catch (error) {
         setloading(false)
         toast.error(`${error.response.data.error} in ${name} category`, {
            autoClose: 2000,
            hideProgressBar: false,
         });
      }
   }

   const selectIdChange = () => {
      setselectedId(null)
      setselectedCategory(null)
   }

   const Loading = () => {
      return <div style={{ display: 'grid', gridTemplateColumns: 'repeat(1, 1fr)', gap: 10 }}>
         {
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
               return <div className='loading-card' style={{ width: '100%' }} key={data + "centersaved"}>
                  <div className='skeleton' style={{ width: "100%", height: 10 }} />
                  <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                  <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                  <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                  <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
               </div>
            })
         }
      </div>
   }

   const handleScroll = (e) => {
      const { scrollTop, clientHeight, scrollHeight } = e.target
      if (scrollTop + clientHeight >= scrollHeight - 5) {
         if (context.savedQuotesDataFinished || context.savedQuotesLoading) return
         context.getSavedQuotes()
      }
   }

   return (
      <div className="center-saved" onScroll={handleScroll}>
         <input id="copySaved" type="text" value="" readOnly={true} />
         {
            context.savedQuotesError && constant.savedQuotesLoading === false ? <div className='error' onClick={() => context.getSavedQuotes()}>Please check your connection and <span>Click here</span> to refresh</div> :
               context.savedQuotes.length === 0 && constant.savedQuotesLoading === false ? <div className='nothing-saved'>You haven't saved anything.</div> :
                  context.savedQuotes.map((data, index) => {
                     if (data.groupName.toLowerCase() === context.filter.toLowerCase()) {
                        return <div className="card" key={"saved" + index}>
                           <p>{data.quote}</p>
                           <span>- {data.author}</span>
                           <div className='button-container'>
                              <div className='info'>
                                 <span>Saved by: {data.userName === undefined ? "Unknown" : data.userName}</span>
                                 <span>Saved on: {getFormatedDate(data.createdAt)}</span>
                                 <div className='groupName'>Saved in: {data.groupName}</div>
                              </div>

                              <div className='buttons'>
                                 <svg onClick={() => selectQuote(data)} style={{ cursor: 'pointer', height: 20, width: 20 }} className='saved' xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill='none'>
                                    <path d="M22 8.86222C22 10.4087 21.4062 11.8941 20.3458 12.9929C17.9049 15.523 15.5374 18.1613 13.0053 20.5997C12.4249 21.1505 11.5042 21.1304 10.9488 20.5547L3.65376 12.9929C1.44875 10.7072 1.44875 7.01723 3.65376 4.73157C5.88044 2.42345 9.50794 2.42345 11.7346 4.73157L11.9998 5.00642L12.2648 4.73173C13.3324 3.6245 14.7864 3 16.3053 3C17.8242 3 19.2781 3.62444 20.3458 4.73157C21.4063 5.83045 22 7.31577 22 8.86222Z" stroke="#000" strokeLinejoin="round" />
                                 </svg>

                                 <svg onClick={() => copyText(data.quote, data.author)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                 </svg>

                                 <svg onClick={() => deleteSavedQuote(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                    <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M10 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M14 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                 </svg>
                              </div>
                           </div>
                           <div style={{ position: 'relative' }}>
                              <SavePopup
                                 loading={loading}
                                 selectId={selectedId}
                                 selectedCategory={selectedCategory}
                                 saveQuoteInGroup={saveQuoteInGroup}
                                 selectIdChange={selectIdChange}
                                 saved={true}
                                 data={data} />
                           </div>
                        </div>
                     } else if (context.filter.toLowerCase() === "all") {
                        return <div className="card" key={"saved" + index}>
                           <p>{data.quote}</p>
                           <span>- {data.author}</span>
                           <div className='button-container'>
                              <div className='info'>
                                 <span>Saved by: {data.userName === undefined ? "Unknown" : data.userName}</span>
                                 <span>Saved on: {getFormatedDate(data.createdAt)}</span>
                                 <div className='groupName'>Saved in: {data.groupName}</div>
                              </div>
                              <div className='buttons'>
                                 <svg onClick={() => selectQuote(data)} style={{ cursor: 'pointer', height: 20, width: 20 }} className='saved' xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill='none'>
                                    <path d="M22 8.86222C22 10.4087 21.4062 11.8941 20.3458 12.9929C17.9049 15.523 15.5374 18.1613 13.0053 20.5997C12.4249 21.1505 11.5042 21.1304 10.9488 20.5547L3.65376 12.9929C1.44875 10.7072 1.44875 7.01723 3.65376 4.73157C5.88044 2.42345 9.50794 2.42345 11.7346 4.73157L11.9998 5.00642L12.2648 4.73173C13.3324 3.6245 14.7864 3 16.3053 3C17.8242 3 19.2781 3.62444 20.3458 4.73157C21.4063 5.83045 22 7.31577 22 8.86222Z" stroke="#000" strokeLinejoin="round" />
                                 </svg>

                                 <svg onClick={() => copyText(data.quote, data.author)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                 </svg>

                                 {
                                    deletedId === data._id ? <div className='loader' /> :
                                       <svg onClick={() => deleteSavedQuote(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                          <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                          <path d="M10 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                          <path d="M14 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                          <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                       </svg>
                                 }
                              </div>
                           </div>
                           <div style={{ position: 'relative' }}>
                              <SavePopup
                                 loading={loading}
                                 selectId={selectedId}
                                 selectedCategory={selectedCategory}
                                 saveQuoteInGroup={saveQuoteInGroup}
                                 saved={true}
                                 selectIdChange={selectIdChange}
                                 data={data} />
                           </div>
                        </div>
                     } else {
                        return null
                     }
                  })
         }

         {context.savedQuotesLoading ? <Loading /> : null}
         <ToastContainer />
      </div>
   )
}

export default CenterSaved
