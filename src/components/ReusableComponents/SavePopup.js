import React, { useContext, useState } from 'react'
import '../ReusableComponentsCss/SavePopup.css'
import AppContext from '../../store/DataProvider'
import CreateCategoryPopup from './CreateCategoryPopup'
import { toast } from 'react-toastify';


const SavePopup = (props) => {

    const context = useContext(AppContext)
    const [showPopup, setshowPopup] = useState(false)

    const togglePopup = () => {
        if (!context.canEdit) return toast(`You have only view permission`, {
            autoClose: 2000,
            hideProgressBar: false,
        });
        setshowPopup((prev) => !prev)
    }

    const saveData = (group, data) => {
        if(props.saved) {
            props.saveQuoteInGroup(group, data.quote, data.author)
        } else {
            props.saveQuoteInGroup(group, data.Queote, data.Author)
        }
    }

    return (
        <React.Fragment>
            {showPopup ? <CreateCategoryPopup togglePopup={togglePopup} /> : null}
            <div className={props.selectId === props.data._id ? "check-list" : "hide"}>
                <div className='head'>
                    <span>Save to...</span>
                    <svg style={{ cursor: 'pointer' }} onClick={props.selectIdChange} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.0014 12.0005L17.244 17.2431M6.75879 17.2431L12.0014 12.0005L6.75879 17.2431ZM17.244 6.75781L12.0014 12.0005L17.244 6.75781ZM12.0014 12.0005L6.75879 6.75781L12.0014 12.0005Z" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </div>

                <div className='button-container'>
                    {
                        context.groupNamesLoading === true ? <span className='loading'>Loading ...</span> :
                            context.groupNames.map((group, index) => {
                                return <button key={index} style={props.selectedCategory === group ? { justifyContent: 'space-between', background: 'rgba(255, 215, 95, 0.1)' } : null}
                                    onClick={()=> saveData(group, props.data)}>{group}
                                    {
                                        props.selectedCategory === group ? props.loading ? <div className='loader' style={{ marginRight: 10 }} /> : <svg style={{ marginRight: 10 }} width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4.16699 10.834L7.50033 14.1673L15.8337 5.83398" stroke="#FFD75F" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg> : null
                                    }
                                </button>
                            })
                    }
                    <button style={{ backgroundColor: '#fff' }} onClick={togglePopup}>Create New Category</button>
                </div>
            </div>
        </React.Fragment>
    )
}

export default SavePopup