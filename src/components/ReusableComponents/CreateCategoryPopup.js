import React, { useState, useContext } from 'react'
import '../ReusableComponentsCss/CreateCategoryPopup.css'
import axios from 'axios'
import constant from '../../constant'
import AppContext from '../../store/DataProvider'
import { toast } from 'react-toastify';

const CreateCategoryPopup = ({ togglePopup }) => {

    const context = useContext(AppContext)
    const [groupName, setgroupName] = useState("")
    const [loading, setloading] = useState(false)

    const createNewGroupName = async () => {
        try {
            if (groupName === "") return toast("Please enter group name")
            if (context.groupNames.includes(groupName)) return toast("Group name all ready existes")
            if (context.groupNames.length === 10) return toast("You can't created more than 10 category")
            setloading(true)
            const organizationId = localStorage.getItem('organizationId')
            await axios.post(`${constant.url}group/create`, {
                organizationId: organizationId,
                groupName: groupName.toLowerCase().trim()
            })

            toast("Category created successfully")
            context.updateGroupName(groupName.toLowerCase().trim())
            setloading(false)
            togglePopup()
        } catch (error) {
            setloading(false)
        }
    }

    const handelCreate = (e) => {
        if (e.key === 'Enter') {
            createNewGroupName()
        }
    }

    return (
        <div className='create-category-popup' onClick={togglePopup}>
            <div className='card1' onClick={(e) => e.stopPropagation()}>
                <div className='head'>
                    <span>Create category</span>
                    <svg style={{ cursor: 'pointer' }} onClick={togglePopup} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.0014 12.0005L17.244 17.2431M6.75879 17.2431L12.0014 12.0005L6.75879 17.2431ZM17.244 6.75781L12.0014 12.0005L17.244 6.75781ZM12.0014 12.0005L6.75879 6.75781L12.0014 12.0005Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>

                <div className='input-container'>
                    <input
                        value={groupName}
                        type="text"
                        placeholder='Create category'
                        onKeyDown={handelCreate}
                        onChange={(e) => setgroupName(e.target.value)} />
                    <button style={{ cursor: 'pointer' }} onClick={loading ? null : createNewGroupName}>{loading ? "Creating . . ." : "Create"}</button>
                </div>
            </div>
        </div>
    )
}

export default CreateCategoryPopup