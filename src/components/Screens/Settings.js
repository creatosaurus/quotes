import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../ScreensCss/Help.css'

const Settings = () => {
    const context = useContext(AppContext)

    return (
        <div className='settings-container'>
            <div className='setting-toggle-container'>
                <button>Plans</button>
            </div>
            <div className='plan-details'>
                <h1>{context?.userInfo?.featureFactoryData?.planId?.planName} plan</h1>
                <div className="plansParaOne">
                    <p>Access to 1 million quotes</p>
                    <button>Yes</button>
                </div>
                <button className="upgradeBtn"
                    onClick={() => window.open("https://www.creatosaurus.io/pricing")}>Upgrade to Increase Limits</button>
            </div>
        </div>
    )
}

export default Settings