import { createContext, useState } from 'react'
import Axios from 'axios'
import constant from '../constant';

const AppContext = createContext({
    //state
    leftSideBarActiveButton: 1,
    quoteQuery: "",
    quotes: [],
    quotesPage: 1,
    quotesLoading: true,
    quotesFinished: false,
    filter: "all",
    canEdit: false,
    userInfo:null,

    groupNames: [],
    groupNamesLoading: true,

    savedQuotes: [],
    savedQuotesPage: 1,
    savedQuotesDataFinished: false,
    savedQuotesError: false,
    savedQuotesLoading: true,

    popularQuotes: [],
    popularQuotesLoading: true,
    getPopularQuotes: () => { },

    //functions
    getUserInformation: () => { },
    changeLeftSideBarAciveButton: () => { },
    getSearchQuotes: () => { },
    changeQuoteQuery: () => { },
    getGroupNames: () => { },
    getSavedQuotes: () => { },
    changeFilter: () => { },
    removeSavedQuote: () => { },
    updateGroupName: () => { },
    updateSavedQuote: () => { }
})

export const AppContextProvider = (props) => {

    const [canEdit, setcanEdit] = useState(false)
    const [leftSideBarActiveButton, setleftSideBarActiveButton] = useState(1)

    const [userInfo, setuserInfo] = useState(null)

    const [quotes, setquotes] = useState([])
    const [quotesPage, setquotesPage] = useState(1)
    const [quotesLoading, setquotesLoading] = useState(true)
    const [quoteQuery, setquoteQuery] = useState("")
    const [quotesFinished, setquotesFinished] = useState(false)

    const [groupNames, setgroupNames] = useState([])
    const [groupNamesLoading, setgroupNamesLoading] = useState(true)

    const [savedQuotes, setsavedQuotes] = useState([])
    const [savedQuotesPage, setsavedQuotesPage] = useState(1)
    const [savedQuotesDataFinished, setsavedQuotesDataFinished] = useState(false)
    const [savedQuotesError, setsavedQuotesError] = useState(false)
    const [savedQuotesLoading, setsavedQuotesLoading] = useState(true)

    const [filter, setfilter] = useState("all")

    const [popularQuotes, setpopularQuotes] = useState([])
    const [popularQuotesLoading, setpopularQuotesLoading] = useState(true)

    const getUserInformation = async () => {
        try {
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            const res = await Axios.get("https://api.app.creatosaurus.io/creatosaurus/user/info", config)

            let userDetails = res.data.activeWorkspace.team.filter(data => data.user_email === res.data.userData.email)
            if (userDetails[0].role !== "view") {
                setcanEdit(true)
            } else {
                setcanEdit(false)
            }

            setuserInfo(res.data)
            localStorage.setItem("organizationId", res.data.activeWorkspace._id)
            localStorage.setItem("organizationName", res.data.activeWorkspace.workspace_name)
            getGroupNames()
            getSavedQuotes()
            getPopularQuotes()
        } catch (error) {
            getGroupNames()
            getSavedQuotes()
            getPopularQuotes()
        }
    }

    const getPopularQuotes = async () => {
        try {
            if (popularQuotes.length !== 0) return
            setpopularQuotesLoading(true)
            const res = await Axios.get(`${constant.url}popular`)
            setpopularQuotes(res.data)
            setpopularQuotesLoading(false)
        } catch (error) {
            setpopularQuotesLoading(false)
        }
    }

    const changeLeftSideBarAciveButton = (value) => {
        setleftSideBarActiveButton(value)
    }

    const getSearchQuotes = async (value) => {
        try {
            setquotesLoading(true)
            setquotesFinished(false)
            setquotes([])
            let query = quoteQuery.trim() === "" ? null : quoteQuery.trim()

            // on cross button call null query
            if (value === "") {
                query = null
            }

            const res = await Axios.get(`${constant.url}search?q=${query}&page=${1}&limit=${18}`)
            setquotesPage(2)
            setquotes(res.data)
            setquotesLoading(false)
        } catch (error) {
            setquotesLoading(false)
        }
    }

    const getSearchQuotesNext = async () => {
        try {
            setquotesLoading(true)
            setquotesFinished(false)
            let query = quoteQuery.trim() === "" ? null : quoteQuery
            const res = await Axios.get(`${constant.url}search?q=${query}&page=${quotesPage}&limit=${18}`)
            setquotesPage((prev) => prev + 1)
            setquotes((prev) => [...prev, ...res.data])
            if (res.data.length === 0) {
                setquotesFinished(true)
            }
            setquotesLoading(false)
        } catch (error) {
            setquotesLoading(false)
        }
    }

    const changeQuoteQuery = (value) => {
        if (value === "") {
            setquoteQuery("")
        } else {
            setquoteQuery(value)
        }
    }

    const sortAlphabetically = (data) => {
        let sortedText = data.sort((a, b) => a.localeCompare(b))
        return sortedText
    }

    const getGroupNames = async () => {
        try {
            if (groupNames.length !== 0) return
            setgroupNamesLoading(true)
            const organizationId = localStorage.getItem('organizationId')
            const res = await Axios.get(`${constant.url}groups/${organizationId}`)
            if (res.data !== "") {
                setgroupNames(sortAlphabetically(res.data.groupName))
            }
            setgroupNamesLoading(false)
        } catch (error) {
            setgroupNamesLoading(false)
        }
    }

    const updateGroupName = (value) => {
        let data = sortAlphabetically([...groupNames, value])
        setgroupNames(data)
    }

    const getSavedQuotes = async () => {
        try {
            setsavedQuotesLoading(true)
            setsavedQuotesError(false)

            const organizationId = localStorage.getItem('organizationId')
            const res = await Axios.get(`${constant.url}saved/${organizationId}?page=${savedQuotesPage}`)
            if (res.data.length < 10) setsavedQuotesDataFinished(true)

            setsavedQuotes((prev) => [...prev, ...res.data])
            setsavedQuotesPage((prev) => prev + 1)
            setsavedQuotesLoading(false)
        } catch (error) {
            setsavedQuotesLoading(false)
            setsavedQuotesError(true)
        }
    }

    const changeFilter = (value) => {
        setfilter(value)
    }

    const removeSavedQuote = (id) => {
        let filterData = savedQuotes.filter(data => data._id !== id)
        setsavedQuotes(filterData)
    }

    const updateSavedQuote = (data) => {
        setsavedQuotes((prev) => [data, ...prev])
    }

    const context = {
        //state
        canEdit: canEdit,
        leftSideBarActiveButton: leftSideBarActiveButton,
        quotes: quotes,
        quotesLoading: quotesLoading,
        quoteQuery: quoteQuery,
        quotesFinished: quotesFinished,

        groupNames: groupNames,
        groupNamesLoading: groupNamesLoading,

        savedQuotes: savedQuotes,
        savedQuotesPage: savedQuotesPage,
        savedQuotesDataFinished: savedQuotesDataFinished,
        savedQuotesError: savedQuotesError,
        savedQuotesLoading: savedQuotesLoading,

        filter: filter,
        userInfo:userInfo,

        popularQuotes: popularQuotes,
        popularQuotesLoading: popularQuotesLoading,
        getPopularQuotes: getPopularQuotes,

        //functions
        changeLeftSideBarAciveButton: changeLeftSideBarAciveButton,
        getSearchQuotes: getSearchQuotes,
        getSearchQuotesNext: getSearchQuotesNext,
        changeQuoteQuery: changeQuoteQuery,
        getGroupNames: getGroupNames,
        getSavedQuotes: getSavedQuotes,
        changeFilter: changeFilter,
        removeSavedQuote: removeSavedQuote,
        updateGroupName: updateGroupName,
        updateSavedQuote: updateSavedQuote,
        getUserInformation: getUserInformation
    }

    return <AppContext.Provider value={context}>
        {props.children}
    </AppContext.Provider>
}

export default AppContext